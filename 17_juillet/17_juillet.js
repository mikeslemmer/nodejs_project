const delaifn = function(callback, temps) {
   if (!temps || temps <= 0) {
      return callback('0 est illegal');
   }
   setTimeout(callback, temps);
};

/*
// format callback
delaifn(
   function(error) {
      if (error) {
         console.log('error!');
         return;
      }
      console.log('fini');   
   },
   1000
);
*/


// format promise
const delaipromise = function(temps) {
   return new Promise(
      function(resolve, reject) {
         delaifn(function(err) {
            if (err) {
               return reject(err);
            }  
            resolve();           
         }, temps);
      }   
   );
};

/*
delaipromise().
   then(function() {
      console.log('promise finie');
   }).
   catch(function(err) {
      console.error('ERROR!', err); 
   }).
   finally(function() {
      console.log('in finally'); 
   });


*/



/*
// format async / await
const delaiasync = async function() {
   console.log('toto2');
   // ici la pile est evidente
   try {
      await delaipromise(500);
      // pile est bizarre parce qu'on est dans un callback
      // meme s'il n'y a pas de callback explicitement ecrit ici!
      await delaipromise();
   } catch(err) {
      console.log('toto3', err);
   }
   console.log('async / await fini');
};

const ret = delaiasync();
console.log('toto', ret);


//

*/
/*
var rp = require('request-promise');

const ajax = async function() {
   const time = await rp('http://worldclockapi.com/api/json/est/now');
   const google = await rp('http://www.google.com?r=' + time.dayOfTheWeek);   
   console.log(google);
};
ajax();

*/


/*
3. Cree une fonction qui:
   A. Cree un repertoire
   B. Cree un fichier dans le repertoire
   C. Enregistre une phrase dans le fichier
   D. Ferme le fichier
   E. Rouvre le fichier et lis le contenu
   F. Imprime le contenu sur l'ecran
   G. Supprime le fichier
   H. Supprime le repertoire.
*/

const fs = require('promise-fs');
/*
const devoirs = async function() {
   try {
      await fs.mkdir('mydir');
      const fd = await fs.open('mydir/myfile', 'w');
      await fs.write(fd, 'some info');
      await fs.close(fd);
      const contenu = await fs.readFile('mydir/myfile');
      console.log(contenu.toString());
      await fs.unlink('mydir/myfile');
      await fs.rmdir('mydir');
   } catch(err) {
      console.error("ERROR", err);
   }
};
devoirs();
*/

fs.mkdir('mydir').
   then(function() {
      return fs.open('mydir/myfile', 'w');
   }).
   then(function(fd) {
      return new Promise(function(resolve, reject) {         
         fs.write(fd, 'some info').
            then(function() {
               resolve(fd); 
            }).
            catch(function(err) {
               reject(err);
            });
      });
   }).
   then(function(fd) {
      return fs.close(fd);
   }).
   then(function() {
      return fs.readFile('mydir/myfile');
   }).
   then(function(contenu) {
      console.log(contenu.toString());
      return fs.unlink('mydir/myfile');
   }).
   then(function() {
      return fs.rmdir('mydir');
   }).
   catch(function(err) {
      console.error("ERROR", err);
   });




