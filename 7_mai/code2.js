let value = 1;

if (value === 1) {
	// oui	
}
else 
{
	// non
}


if (value) {

} 
else 
{

}

value = 0;
while(value) {
	console.log(value);
	value--;
}


// peu utilise
do {
	console.log(value);
	value--;
} while(value);


let tableau = [1, 2, 3, 4, 5];
while(tableau.length > 0) {
	console.log(tableau.pop());
}

for (value = 0; value <= 5; value++) {
	console.log(value);
}

while (true) {
	console.log('hello');	
	break;
}

// meme chose
for (;;) {
	console.log('hello');
}

console.log([1,2,3,4,5].pop()); // ---> 5
[1,2,3,4]

value = [1,2,3,4,5];
while(true) {
	const elem = value.pop();
	if (elem === 3) {
		break;
	}	
}

console.log(value);
console.log(elem);


value = [1,2,3,4,5];

const fn = function(elem) {
	console.log(elem);
};

for (let i = 0; i < value.length; i++) {
	fn(value[i]);
}

value.forEach(
	fn
);




const faire_deux_fois = function(autre_fn) {
	// il faut que typeof(autre_fn) soit 'function'
	autre_fn();
	autre_fn();
}



const moins_que = function() {
	const a = arguments[0];
	const b = arguments[1];
	console.log(a, b);
	if (a == b) {
		return 0;
	}
	if (a < b) {
		return -1;	
	}		
	// ici, b < a
	return 1;
};

let x = ['cheval', 'chien', 'chat', 'vache'];
x.sort(moins_que);




const z = [1,2,3,4,5];

const z2 = z.map(
	function(elem) {
		return elem + 1;
	}
);
// z2 est [2,3,4,5,6]

const z3 = z.forEach(
	function(elem) {
		return elem + 1;
	}
);
// z3 est undefined parce que forEach ne cree rien

const z5 = z.reduce(
	function(prev, elem) {
		return prev + elem;
	}, 
	''
);


const z6 = z.filter(
	function(elem) {
		return elem > 2;
	}
);

// syntaxe hipster
const z7 = z.filter(
	(elem) => {
		return elem > 2;	
	}
);

// syntaxe veillie
function my_global_function() {
	console.log('hello');
}

// function dans le scope local
while (true) {
	const my_global_function = function() {
		console.log('hello');
	};
}


let test = (3 > 1 ? 'yes' : 'no');
// test == 'yes'

let test;
if (3 > 1) {
	test = 'yes';
} else {
	test = 'no';
}

console.log('hello ' + (3 > 1 ? 'yes' : 'no') );

// || ou
// && et

const rebels = pilots.filter(pilot => pilot.faction === "Rebels");
const rebels = pilots.filter(
	function(pilot) { return pilot.faction === "Rebels"); }
);













// exemple en ligne

var personnel = [
  {
    id: 5,
    name: "Luke Skywalker",
    pilotingScore: 98,
    shootingScore: 56,
    isForceUser: true,
  },
  {
    id: 82,
    name: "Sabine Wren",
    pilotingScore: 73,
    shootingScore: 99,
    isForceUser: false,
  },
  {
    id: 22,
    name: "Zeb Orellios",
    pilotingScore: 20,
    shootingScore: 59,
    isForceUser: false,
  },
  {
    id: 15,
    name: "Ezra Bridger",
    pilotingScore: 43,
    shootingScore: 67,
    isForceUser: true,
  },
  {
    id: 11,
    name: "Caleb Dume",
    pilotingScore: 71,
    shootingScore: 85,
    isForceUser: true,
  },
];

var totalJediScore = personnel
  .filter(function (person) {
    return person.isForceUser;
  })
  .map(function (jedi) {
    return jedi.pilotingScore + jedi.shootingScore;
  })
  .reduce(function (acc, score) {
    return acc + score;
  }, 0);



const totalJediScore = personnel
  .filter(person => person.isForceUser)
  .map(jedi => jedi.pilotingScore + jedi.shootingScore)
  .reduce((acc, score) => acc + score, 0);
