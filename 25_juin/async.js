// Les 3 syntaxes font exactement la meme chose!


// 1. Avec des callbacks
const func1 = function() {
   console.log('1');
   oper(function() {
      console.log('3');   
   });
};
func1();
console.log('2');



// 2. Avec des 'promises'
const func2 = function() {
   console.log('1');
   oper().
      then(function() {
         console.log('3');
      }); 
   
};
func2();
console.log('2');




// 3. Avec async/await
const func3 = async function() {
   console.log('1');
   await oper();
   console.log('3');
};
func3();
console.log('2');





//////// callback hell:

const cree_repertoire(callback) {
   // Fait des operations
   callback();
}

cree_repertoire(
   function() {
      cree_fichier(
         function() {
            ecrit_donnees_dans_le_fichier(
               function() {
                    ferme_le_fichier(
                        function() {                           
                        }                    
                    );                      
               }
            );             
         }
      );     
   }
);



const cree_repertoire() {
   // resolve et reject sont 2 fonctions qui sont appelees
   // par notre fonction quand on a fini
   return new Promise(function(resolve, reject) {
      // Ici on cree la repertoire
      
      // Si ca marche, on appelle resolve:
      // Les parametres de resolve vont etre donnes a la prochaine 
      // fonction dans la chaine (le prochain 'then')
      resolve();
      
      // Sinon, on appelle reject.
   });
}

cree_repertoire().
   then(function() {
      return cree_fichier();   
   }).
   then(function() {
      return ecrit_donnees();   
   }).
   then(function() {
      return ferme_le_fichier(); 
   }).
   error(function() {
      
   });
   
   
   
// avec await/async

const fn = async function() {
   await cree_repertoire();
   await cree_fichier();
   await ecrit_donnees();
   await ferme_le_fichier();
};

fn();




