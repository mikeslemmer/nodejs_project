1.

CREATE TABLE Language
(
   name CHAR(30),
   latin ENUM('T', 'F') NOT NULL DEFAULT 'F',
   PRIMARY KEY (name) 
);


INSERT INTO Language (`name`)
   SELECT DISTINCT language FROM countrylanguage;


ALTER TABLE countrylanguage
   ADD CONSTRAINT `countryLanguage_ibfk_2` FOREIGN KEY(language) REFERENCES Language(name);


UPDATE Language
SET latin ='T'
WHERE Language.name IN ('French' , 'Italian' , 'Spanish' , 'Portuguese' , 'Romansh' , 'Romanian');


 
 
2.

SELECT country.name, countrylanguage.language, countrylanguage.Isofficial
FROM country
INNER JOIN
countrylanguage
ON country.code = countrylanguage.countrycode
INNER JOIN
Language
ON countrylanguage.language=Language.name
WHERE countrylanguage.Isofficial='T'
AND 
Language.latin='T';


3. 







CREATE TABLE `Favcity` (
   CountryCode CHAR(3) NOT NULL DEFAULT '',
   cityid int,
   KEY `cityid` (`cityid`),
   CONSTRAINT `cityfav_fk_1` FOREIGN KEY (`cityid`) REFERENCES `city` (`ID`)
);


CREATE TABLE `Favcity` (
   cityid int NOT NULL,
   CONSTRAINT `cityfav_fk_1` FOREIGN KEY (`cityid`) REFERENCES `city` (`ID`)
);




CREATE TABLE `FavCountry` (
   CountryCode CHAR(3) NOT NULL,
   cityid int NOT NULL,
   PRIMARY KEY (`CountryCode`),   
   CONSTRAINT `fc_fk_1` FOREIGN KEY (`CountryCode`) REFERENCES `country` (`Code`),
   CONSTRAINT `fc_fk_2` FOREIGN KEY (`cityid`) REFERENCES `city` (`ID`)
);


INSERT INTO `FavCountry` VALUES ('USA',3839);
INSERT INTO `FavCountry` VALUES ('NLD',30);
INSERT INTO `FavCountry` VALUES ('ITA',1464);
INSERT INTO `FavCountry` VALUES ('FRA',2978);
INSERT INTO `FavCountry` VALUES ('IND',1036);





 
 4.
 
 
EXPLAIN SELECT 
   country.code, country.population, FavCountry.cityid
FROM 
   country
LEFT OUTER JOIN 
   FavCountry
ON 
   country.code = FavCountry.countrycode
WHERE 
   FavCountry.cityid IS NULL
ORDER BY 
   country.population
DESC
LIMIT 1;




SELECT country.code FROM country
WHERE country.code NOT IN (
   SELECT CountryCode FROM FavCountry
)
ORDER BY population DESC LIMIT 1;



SELECT DISTINCT country.code FROM country
  WHERE NOT EXISTS (SELECT * FROM FavCountry
                    WHERE FavCountry.CountryCode = country.code);

 
 
 
 
 
 
 