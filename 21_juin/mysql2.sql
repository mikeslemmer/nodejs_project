SELECT 
   `city`.`name`, `city`.`population`, `francophone_country_codes`.`biggest`
FROM
(
   SELECT 
      MAX(`city`.`population`) AS `biggest`
   FROM 
      `country` 
   INNER JOIN 
      `countrylanguage`
   ON 
      `country`.`code` = `countrylanguage`.`countrycode`	
   INNER JOIN
      `city`
   ON
      `country`.`capital` = `city`.`id`
   WHERE
      `countrylanguage`.`language` = 'French'
) AS `francophone_country_codes`
INNER JOIN
   `city`
ON
   `francophone_country_codes`.`biggest` = `city`.`population`












SELECT 
	cl.language, AVG(lifeExpectancy)
FROM 
	countrylanguage AS cl
INNER JOIN
	country
ON
	cl.countrycode=country.code
WHERE
	cl.language ="French" OR cl.language="English"
GROUP BY
	cl.language
;




-- cl.language='English'
-- AND 


SELECT distinct country.name FROM country
INNER JOIN countrylanguage AS cl
ON country.code = cl.countrycode
WHERE 
cl.language='English'
AND 
country.name IN 
(
SELECT 
	 c.name as cn
FROM 
	countrylanguage AS cl
INNER JOIN 
	country AS c
ON 
	cl.countrycode=c.code
WHERE 
	cl.language= 'French'
);




SELECT 
   country.name
FROM 
	countrylanguage AS cl
INNER JOIN 
	countrylanguage cl2
ON 
	cl.countrycode = cl2.countrycode
INNER JOIN
   country
ON
   cl.countrycode = country.code
WHERE 
	cl.language= "French" 
AND
   cl2.language="English"
;



SELECT 
   countrylanguage.countrycode
FROM 
   countrylanguage
WHERE
   countrylanguage.language = 'French'
OR
   countrylanguage.language = 'English'
GROUP BY
   countrylanguage.countrycode
HAVING
   count(*) = 2
;



SELECT 
   countrylanguage.countrycode,
   SUM(
      CASE countrylanguage.language 
         WHEN 'French' THEN 1
         WHEN 'English' THEN 1 
         ELSE 0 
      END   
   ) AS cnt
FROM 
   countrylanguage
GROUP BY
   countrylanguage.countrycode
HAVING
   cnt = 2
;


-- CA NE MARCHE PAS:

SELECT
   `country`.`name`,
   `city`.`name`
FROM
   `city`
INNER JOIN
   `country`
ON
   `city`.`countrycode` = `country`.`code`
INNER JOIN
(
   SELECT 
      `city`.`countrycode` AS `cc`,
      MAX(`city`.`population`) AS `p`
   FROM
      `city`
   GROUP BY `countrycode`
) AS `biggest`

ON   
   `country`.`code` = `biggest`.`cc`
AND
   `city`.`population` = `biggest`.`p`

UNION

(
   SELECT
      `city`.`countrycode` AS `cc`,
      MAX(`city`.`population`) AS `p`,
   FROM
      `city`
   WHERE
      `p` NOT IN
      (
         SELECT 
            MAX(`city`.`population`) AS `p2`
         FROM
            `city`
         GROUP BY `countrycode`    
      )   

   
   GROUP BY `countrycode`   
)   
;






