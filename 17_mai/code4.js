
class Forme {
   constructor(x, y, visible, couleur) {
      if (x === undefined || y === undefined || 
			 visible === undefined || couleur === undefined) {
			throw('parameters missing');
      }

      this.x = x;
      this.y = y;
      this.visible = visible;
      this.couleur = couleur;      
   }
  
	deplace(x, y) {
		this.x = x;
		this.y = y;
	}  
	
	cache() {
		this.visible = false;
	}
	
	affiche() {
		this.visible = true;
	}
	
	dessine() {
		throw "broken";
	}
}


class Rectangle extends Forme {
	constructor(x, y, visible, w, h, couleur) {
		super(x, y, visible, couleur);
		this.w = w;
		this.h = h;
	}	

	static distance(x1, x2) {
		return x2 - x1;
 	}
 	
	agrandis(w, h) {
		this.w = w;
		this.h = h;
	}
	
	dessine() {
		console.log(this);
	}
}

Rectangle.distance(5, 3); // return == 2

class Carre extends Rectangle {
	constructor(x, y, visible, t, couleur) {
		super(x, y, visible, t, t, couleur);	
	}
	
	agrandis(t) {
		super.agrandis(t, t);
	}	
}


const r = new Rectangle(0, 0, true, 100, 100, 'vert');
r.dessine();
r.deplace(50, 50);
r.dessine();
r.agrandis(200, 200);
r.dessine();

try {
//   asdljf; alkjadskj;lk
	try {
  	   JSON.parse('asjdfaksljfddf');
	    asdljf; alkjadskj;lk
	   ;lksaj;fjdsf;j;fjkdfj;
	} 
	catch(e) {
		console.log('catch 1');
	}
}
catch(e) {
	console.log('catch 2');
}
finally {
	console.log('finally!');
}

const r2 = new Rectangle(0, 0, true, 100, 100);




// syntaxe vieillie pour les classes/objets
const FormeVieille = function(x, y, visible, couleur) {
	this.x = x;
	this.y = y;
	this.visible = visible;
	this.couleur = couleur;
};

//FormeVieille['prototype']['deplace'] = function(x, y) { ...
FormeVieille.prototype.deplace = function(x, y) {
	this.x = x;
	this.y = x;
};

const fv = new FormeVieille(0, 0, false, 'vert');
fv.deplace(10, 10);






class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  static distance(a, b) {
    const dx = a.x - b.x;
    const dy = a.y - b.y;
    return Math.hypot(dx, dy);
  }
  
  distanceTo(a) {
	  const dx = a.x - this.x;
	  const dy = a.y - this.y;
	  return Math.hypot(dx, dy);  
  }
}

const distance2 = function(a, b) {
    const dx = a.x - b.x;
    const dy = a.y - b.y;
    return Math.hypot(dx, dy); // sqrt(dx * dx + dy * dy)
}

const p1 = new Point(5, 5);
const p2 = new Point(10, 10);

console.log(Point.distance(p1, p2)); // 7.0710678118654755
console.log(distance2(p1, p2));
console.log(p1.distanceTo(p2));



