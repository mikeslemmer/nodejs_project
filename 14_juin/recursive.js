


const factoriel = function(x) {
	let ret = 1;
	let i;
	for (i = 1; i <= x; i++) {
		//ret = ret * i;
		ret *= i;
	}
	return ret;
}


const factoriel_recursive = function(x) {
	console.log(x);
	if (x == 0) {
		return 1;
	}
	return x * factoriel_recursive(x - 1);
}

// 0 -> 1
// 1 -> 1 * factorial_recursive(0) -> 1 * 1 -> 1
// 2 -> 2 * factorial_recursive(1) -> 2 * 1 * 1 -> 2

// fr(10) -> fr(9) -> fr(8) -> fr(7)
/*

[10, 5, 15, 20, 25, 30, 12]
						     15
						     /\
					10         	 	 25
				 /		\				/	\
			5			  12		20			30
*/
// 1 -> 1
// 2 -> 3  (2 ^ 2 - 1)
// 3 -> 7  (2 ^ 3 - 1)
// 10 -> 1023  (2 ^ 10 - 1)
// 1000 elements -> hauteur de l'arbre de 10
// 1000000 elements -> 20

class tree_elem {
	constructor(parent, val) {
		this.parent = parent;
		this.val = val;
		this.gauche = null;
		this.droite = null;
	}
	
// val == 5
// gauche == null
// droite == null	
	
	ajoute(val) {
		if (val == this.val) {
			throw "Valeur deja dans l'arbre";
		}
		if (val < this.val) {
			if (this.gauche) {		// meme chose que: if (this.gauche != null)
				this.gauche.ajoute(val);
			} else {
				this.gauche = new tree_elem(this, val);			
			}		
		} else {
			if (this.droite) {
				this.droite.ajoute(val);				
			} else {
				this.droite = new tree_elem(this, val);
			}
		}
	}
	
	cherche(val) {
		console.log(this); 	
		if (val == this.val) {
			return  true;
		}
		if (val < this.val) {
			if (this.gauche) {		// meme chose que: if (this.gauche != null)
				return this.gauche.cherche(val)   ;
			} else {
				return false; //this.gauche = new tree_elem(this, val);			
			}		
		} else {
			if (this.droite) {
				return this.droite.cherche(val);				
			} else {
				return false; // this.droite = new tree_elem(this, val);
			}
		}
	}		
		

	
}

class tree {
	constructor() {
		this.racine = null;	
	}
	
	ajoute(val) {
		if (this.racine == null) {
			this.racine = new tree_elem(null, val)
		} else {	
			this.racine.ajoute(val);
		}
	}
	cherche(val) {
				
		if (this.racine == null) {
			return false; 		
		} else  {
			return this.racine.cherche(val);
		}
	}	
	
	efface(val) {
		throw('pas encore implemente');
	}
}



const t = new tree();
t.ajoute(5);
t.ajoute(8);


