var stuff;

const otherstuff = 57;
//otherstuff = 22; 			// Erreur

let another = 57;
another = 22.55;
another = 22;

console.log(another);
console.log(typeof(another));

// maintenant c'est 22

another = "bonjour";
console.log(typeof(another));


another = true;

another = [['x', 'y', 'z'], 2, 3, 4, ['a', 'b', 'c']];
console.log(another[0][1]);
console.log(another[1]);
// console.log(another.1); // illegal


another = {
	premier: 1,
	second: 2,
	autre: 3
};

console.log(Object.keys(another));
console.log(Object['keys'](another));

console.log(another['premier']);
console.log(another.premier);
console.log(another['0']);





let colorobj = {
  "colors": [
    {
      "color": "black",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [255,255,255,1],
        "hex": "#000"
      }
    },
    {
      "color": "white",
      "category": "value",
      "code": {
        "rgba": [0,0,0,1],
        "hex": "#FFF"
      }
    },
    {
      "color": "red",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [255,0,0,1],
        "hex": "#FF0"
      }
    },
    {
      "color": "blue",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [0,0,255,1],
        "hex": "#00F"
      }
    },
    {
      "color": "yellow",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [255,255,0,1],
        "hex": "#FF0"
      }
    },
    {
      "color": "green",
      "category": "hue",
      "type": "secondary",
      "code": {
        "rgba": [0,255,0,1],
        "hex": "#0F0"
      }
    },
  ]
};

// [] crochet  bracket
// {} accolade  curly-brace


console.log(colorobj.colors[2].code.rgba[3]);
console.log(colorobj['colors'][2]['code']['rgba'][3]);

console.log(console);
console.log(Object);

