class AfficheData {
	static get_list() {
		// "AJAX" request
		// "Asynchronous Javascript And XML"
		// telecharge http://localhost:8000/list	
		$.get("/list")
		.done(
			function(data) {
				data.forEach(
					function(elem) {
						AfficheData.get_article(elem);
					}
				);
			}
		)
		.fail(
			function(err) {
				console.error(err);
			}		
		 );
	}
	
	static get_article(id) {
		$.get("/article?id=" + id)
		.done(
			function(data) {
				$('#articles').append(
					'<div id="' + id + '" class="article">' + 
					'<div id="article-' + id + '" class="article-text">' + data.article + '</div>' + 
					'<div id="author-' + id + '" class="author">' + data.auteur + '</div>' + 
					'</div>'
				);				
				
				
			}
		)
		.fail(
			function(err) {
				console.error(err);
			}		
		 );	
	}
}


// "SOP" = "Same-origin policy"
// $.get('http://www.google.com/toto')
// $.get('http://mail.google.com/your_email_box')
// $.get('/hello')


$(document).ready(
	function() {
		$('#telecharge').click(
			function() {
				$('#articles').html('');
				AfficheData.get_list();
			}
		);
	}
);

