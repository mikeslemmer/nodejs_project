//const regex = new RegExp("xyz");
const regex = /xyz/;
const regex_cherche_au_debut = /^xyz/;
const regex_cherche_a_la_fin = /xyz$/;
const regex_les_deux = /^xyz$/;
const regex_x_y_ou_z = /[xyz]/;
const regex_debut_x_y_ou_z = /^[xyz]/;
const regex_pas_x_y_ou_z = /[^xyz]/;
const regex_intervalle = /[a-z][A-Z][0-9]/;
const regex_point_interrogation = /^x?y?$/;
const regex_etoile = /^x*y*$/;
const regex_plus = /^x+y+$/;   // x+ est la meme chose que xx*
const regex_entre_2_et_5 = /^x{2,5}$/;
const regex_options = /xyz/i;
const regex_blanc = /a\s*b/;
const regex_pas_blanc = /a\S*b/;
const regex_mot = /a\wb/;  // \w === [a-zA-Z]
const regex_pas_mot = /a\Wb/; // PAS [a-zA-Z]
const nimporte_quoi = /const.+hello/;
const parenthese = /const(.+)hello/;

const chaine = "sblxyzask\nfjfxyz\nxyzlk\nxyz\nxxxxdX4xxxx";
const tableau = chaine.split('\n');
const output =
	tableau.filter(
		function(elem) {
			return regex_intervalle.test(elem);
		}
	);
console.log(output);


const chaine = "je suis mike et je veux te voir maintenant."
const regex_test = /^je suis (\w+) et (\w+) veux te voir maintenant\.$/;
regex_test.test(chaine);
const response = chaine.match(regex_test);






