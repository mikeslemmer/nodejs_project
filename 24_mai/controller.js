/*
	"REST"
	GET		// acceder aux donnees
	POST		// creer des nouvelles donnees
	PUT		// changer des donnees "update"
	DELETE	// supprimer des donnees
	Ce sont les "verbes HTTP"
*/

const model = require('./model');

class Controller {

	constructor() {
		throw 'static class';
	}
	
	static get_list(request, response) {				
		response.send(model.get_list());
	}

	static get_article(request, response) {
		const id = request.query['id'];
		if (!id) {
			return response.sendStatus(403);
		}
		const article = model.get_article(id);
		if (!article) {
			return response.sendStatus(404);		
		}		
		response.send(article);		
	}
}

module.exports = Controller;


