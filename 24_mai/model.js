class Model {	
	constructor() {
		this.data = require('./data');
	}
	
	// trouve tous les id des articles dans la
	// base de donnees
	// return: [1000, 1001]
	get_list() {
		// map, reduce, filter, forEach
		return this.data.articles.map(
			function(elem) {
				return elem.id;
			}
		);
	}

	get_article(id) {
		// balayer le tableau pour trouver l'element
		// avec elem.id === id. Si oui, cet element
		// sera dans le retour. Sinon, non.
		const filtre = this.data.articles.filter(		
			// La function qui decele
			function(elem) {
				return elem.id == id;
			}
		);
		
		// En fait toutes les trois expressions
		// font la meme chose
		return filtre[0] || null;
//		return filtre[0] ? filtre[0] : null;
//		return (filtre.length > 0) ? filtre[0] : null;
	}
}

/*
const model = new Model();
//const article = model.get_article(1005);
//console.log(article);
//const list = model.get_list();
//console.log(list);
const first = model.get_article(model.get_list()[0]);
console.log(first);

model.data.articles[0];
*/

module.exports = new Model();
