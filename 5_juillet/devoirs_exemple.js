// https://nodejs.org/api/fs.html
"use strict";
const fs = require('fs');

console.log('open commence');
fs.open('promise.js', 'r', 
   function(err, fd) {
      console.log('read commence');
      const buf = new Buffer(1000);
      fs.read(fd, buf, 0, 100, 0,
         function(err, bytesRead) {         
            console.log('close commence');                        
            fs.close(fd, 
               function(err) {
                  console.log(buf.toString());               
               }          
            ); 
            console.log('close fini');            
         }
      );
      console.log('read fini');
   }
);
console.log('open fini');


////////
/// Avec des fonctions et des variables globales
////////

let buf = null;
let fd = null;

const afficheResultat = function(err) {
   console.log(buf.toString());               
};

const fermeFichier = function(err, bytesRead) {
   fs.close(fd, afficheResultat);
};

const lireFichier = function(err, fd_input) {
   buf = new Buffer(1000);
   fd = fd_input;
   fs.read(fd, buf, 0, 100, 0, fermeFichier);
   return lireFichier;
};

fs.open('promise.js', 'r', lireFichier);


//////////////
// Passer les parametres avec des
// fonctions anonymes.
//////////////


const afficheResultat = function(err, buf) {
   console.log(buf.toString());               
};

const fermeFichier = function(err, bytesRead, fd, buf) {
   fs.close(fd, function(err) {
      afficheResultat(err, buf);
   });
};

const lireFichier = function(err, fd) {
   const buf = new Buffer(1000);  
   fs.read(fd, buf, 0, 100, 0, function(err, bytesRead) {
      fermeFichier(err, bytesRead, fd, buf);
   });
};

fs.open('promise.js', 'r', lireFichier);







