

const fnAdd = function (a, b, fncallbk) {
   const somme = a + b;   
   fncallbk(somme);
};
      
const affSomme = function (c) {
   console.log('BONJOUR', c);
};

fnAdd(5, 4, affSomme);










const affSomme = function (c) {
   console.log('bonjour', c);
};

const fnAdd = function (a, b, fncallbk) {
   somme = a + b;
   fncallbk(somme);
   return somme;   
};

 
fnAdd(5, 4, affSomme);  // -- erreur affSomme n'est pas connu
fnAdd(5, 4, function(c) {
   console.log(c);
});

fnAdd(5, 4, console.log);





const fnAdd2 = function (a, b, fn) {
   fn(a + b);
};

const affSom = function (c) {
   console.log(c);
};

console.log(fnAdd2(5, 4, affSom));



fnAdd2 -> ligne 44
   affSom -> appele avec parametre (a + b)
      ligne 48 -> affiche c
      ligne 49 -> renvoie c
   renvoie ce que affSom m'a renvoye. 
 



const a = function() {
   return 7;
};

const b = function() {
   a();
};

const c = function() {
   return b();
};

const d = c();
// Q: Quelle valeur contient d maintenant?


c -> b         ->  a
     undefined  <- 7
          















const b = function() {
   a();
};






( x ) => x;

function(x) { 
   return x;
}



////////////////////////

(a + b) * c

///////////////////////

const add = function(a, b) {
   return a + b;
};

const mult = function(a, b) {
   return a * b;
};

console.log(mult(add(a, b), c));

//////////////////////

const add = function(a, b, callback) {
   callback(a + b);
};

const mult = function(a, b, callback) {
   callback(a * b);
};

add(a, b, function(res) { 
   mult(res, c, console.log);
});












const fnAdd2 = function (a, b, fn) {
   const somme = a + b;
   fn(somme);
};
   
const affSom = function (c) {
   console.log(c);
};
   
fnAdd2(5, 4, affSom);










const fs = require('fs');
const fn1 = function() {
   // ici on est dans fn1, fn2, et main
   
   const stack = new Error().stack;
   console.log("STACK 1", stack );
 
   fs.open('fichier', 'r',
      function(err, result) {
     
         const stack2 = new Error().stack
         console.log("STACK 2", stack2 );
               
         throw('error');   
      }
   );
};


const fn2 = function() {
   try {
      fn1();
   } catch {
      console.log("GOT HERE");   
   }
};


const main = function() {
   fn2();
};


main();





