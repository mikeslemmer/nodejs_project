
/*
2. Fait la meme chose mais avec (a / b) - et il y a une 
   erreur si b == 0. Gere-ca avec reject pour la promise et
   avec un deuxieme callback pour les callbacks. Et j'ai
   aucune idee comment ca marche pour async/await
   
   (note: pour les callbacks le norm est: function(err, result) {})
*/
   

//=============================================
// Faire une addition avec une fn Callback
//=============================================

const fnAdd = function (a,b,fncallbk) {
   return(a+b);
   fncallbk();
};
      
const affSomme = function (c) {
   console.log(c);
};
   
fnAdd(5,4,affSomme);

//=============================================
// Faire une addition avec une fn Callback (Erreur)
//=============================================

const fnAdd= function (a,b,fncallbk) {
      somme=(a+b); return somme;
      var affSomme =function (c) {
         console.log(c);
         };
      fncallbk();
      };
    
   
//==> fnAdd(5,4,affSomme);  -- erreur affSomme n'est pas connu

//=============================================

const fnAdd2 = function (a,b,fn) {
   return(a+b);
   fn();
   };
   const affSom=function (c) {
   console.log(c);
   };
   
fnAdd2(5,4,affSom) ;

//=> OK

//=============================================
--Fonctions chainées avec PROMISE (addition)
//=============================================

"use strict";
const fnAdd2 = function(a, b) {
   return new Promise(
         function(resolve,reject) {
             return resolve(a + b);         
         });
};

fnAdd2(3,4).
   then(function(c) {
      console.log(c);
   });
 
   

//=============================================
// fonction a/b avec Callback
// Vers.1
//=============================================


const fnDiv = function (a, b, callbk) {
   if (b == 0) {
      callbk('erreur b doit etre different de 0', null);
      return;
   }
   callbk(null, a / b);
}
const affDiv = function (err, res) {
   if (err) {
      console.error(err);  
   } else {
      console.log(res);
   }   
}

fnDiv(5,2,affDiv);
    
//=============================================
// fonction a/b avec Callback
// Vers.2
//=============================================

const fnDiv = function (a, b, callbk) 
{
   if (b == 0) 
   {
      return callbk("le dénominateur ne doit pas être nul", null);     
   } 
   else 
   {
      callbk(null, a / b);
   }
}


const affDiv = function (erreur,c) 
{
   if (erreur) 
   {
      return console.error ("erreur : "+ erreur);
   }
   console.log(c);
}

fnDiv(5,0,affDiv);

// pb = la division par 0 n'est pas une erreur pour node



//=============================================
// fonction a/b avec Promise
// Vers.1
//=============================================

"use strict";
const fnDiv = function(a,b)
{
   return new Promise(
      function(resolve, reject)
      {
         if (b==0) 
         {
            return reject('la division par 0 est impossible');
         }
                  
         resolve(a / b);
      }
   );
}


fnDiv(6, 3).
   then(
      function(d)
      {
         console.log(d);   
      }).
   catch(
      function(err)
      {
         console.log(err);
      });



//=============================================
// fonction a/b avec Promise
// Vers.2
//=============================================

"use strict";
const fnDiv2 = function(a, b) 
{
   return new Promise(
      function(resolve, reject)
      {
         if (b == 0)  {
            return reject('la division par 0 est impossible');
         }
         resolve(a / b);
      }
   );
};
  
  
fnDiv2(6,2).
      then(
         function(result){
            console.log(result);
         }
      ).
      catch(
         function(err) {
            console.log ("c'est une erreur");
            console.log(err); 
         }
      );


