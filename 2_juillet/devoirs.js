1. Cree une fonction qui additione deux nombres et appelle
   une fonction avec la reponse des qu'elle a fini.
   A. Avec callback
   B. Avec Promise
   C. Avec async/await

   ex sync:
   const fn = function(a, b) { return a + b; };
   
2. Fait la meme chose mais avec (a / b) - et il y a une 
   erreur si b == 0. Gere-ca avec reject pour la promise et
   avec un deuxieme argument pour les callbacks. Et j'ai
   aucune idee comment ca marche pour async/await
   
   (note: pour les callbacks le norm est: function(err, result) {})
   
   
3. Cree une fonction qui:
   A. Cree un repertoire
   B. Cree un fichier dans le repertoire
   C. Enregistre une phrase dans le fichier
   D. Ferme le fichier
   E. Rouvre le fichier et lis le contenu
   F. Imprime le contenu sur l'ecran
   G. Supprime le fichier
   H. Supprime le repertoire.
   
// Si tu veux, fais-ca avec le pyramid of doom et apres avec promise et async/await   
// Sinon, fais-le avec que un systeme (tu peux choisir)