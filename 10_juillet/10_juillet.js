// https://nodejs.org/api/fs.html
"use strict";
const fs = require('fs');

console.log('open commence');
fs.open('promise.js', 'r', 
   function(err, fd) {
      console.log('read commence');
      const buf = new Buffer(1000);
      fs.read(fd, buf, 0, 100, 0,
         function(err, bytesRead) {         
            console.log('close commence');                        
            fs.close(fd, 
               function(err) {
                  console.log(buf.toString());               
               }          
            ); 
            console.log('close fini');            
         }
      );
      console.log('read fini');
   }
);
console.log('open fini');


//======================================================================

///
//  fs.open(path, flags, callback)
//


//fs.mkdir(nom de rep., callback);

/*
   
3. Cree une fonction qui:
   A. Cree un repertoire
   B. Cree un fichier dans le repertoire
   C. Enregistre une phrase dans le fichier
   D. Ferme le fichier
   E. Rouvre le fichier et lis le contenu
   F. Imprime le contenu sur l'ecran
   G. Supprime le fichier
   H. Supprime le repertoire.
*/   
// Si tu veux, fais-ca avec le pyramid of doom et apres avec promise et async/await   
// Sinon, fais-le avec que un systeme (tu peux choisir)

/* 
fs.mkdir(path[, mode], callback)

    path <string> | <Buffer> | <URL>
    mode <integer> Not supported on Windows. Default: 0o777.
    callback <Function>
        err <Error>

Asynchronously creates a directory. No arguments other than a possible exception are given to the completion callback.
*/

//======================================================

"use strict";
const fs = require('fs');

fs.mkdir(
   'dossier1', function(err) {
      fs.open(
         './dossier1/fichier1', 'a+', function(err) {
            fs.appendFile(
               './dossier1/fichier1', 'blablabla\n', function(err) {
                  console.log('fichier ecrit');
                  }         
               );      
            }
       )  
   }
);



//fs.write(fd, string[, position[, encoding]], callback)
/*
History

    fd <integer>
    string <string>
    position <integer>
    encoding <string>
    callback <Function>
        err <Error>
        written <integer>
        string <string>
        
        
 fs.close(fd, callback)
   callback <Function>
        err <Error>
*/

const do_it = function(dossier, fichier, texte) {
  fs.mkdir(
    dossier, function(err) {
      fs.open(
         dossier + '/' + fichier, 'a+', function(err, fd) {
            fs.write(
               fd, texte, function(err, written, string) {
                  fs.close( 
                     fd, function(err) {
                        console.log("c'est fait");
                     }
                  );
               }
            );
         }
      );  
    }
  );
};





//======================================================
  

"use strict";
const fs = require('fs');

fs.mkdir(
   'dossier1', function(err) { 
      fs.open(
         './dossier1/fichier1', 'a+', function(err, fd) {
            fs.appendFile(
               './dossier1/fichier1', ' blablabla \n', function(err) {
                  console.log('1ere ligne ecrite dans le fichier');   
                  fs.appendFile(
                     './dossier1/fichier1', ' tototo ', function(err) {
                        console.log('2eme ligne ecrite');
                     }
                  );         
               }         
            )      
         }
      );  
   }
);


//======================================================
  


"use strict";
const fs = require('fs');
fs.mkdir(
   'dossier1', function(err) {
      fs.open(
         './dossier1/fichier1', 'a+', function(err, fd) {
            fs.appendFile(
               './dossier1/fichier1', ' blablabla \n', function(err) {
                  console.log('1ere ligne ecrite dans le fichier');   
                  fs.appendFile(
                     './dossier1/fichier1', ' tototo ', function(err) {
                        console.log('2eme ligne ecrite');
                        fs.close(
                           fd, function(err) {
                              fs.open(
                                 './dossier1/fichier1', 'r', function(err, fd) {
                                    console.log("fichier ouvert");
                                 } 
                              );
                           }
                        );
                     }
                  );         
               }         
            );      
         }
      );  
   }
);


//======================================================
  










/*

   fs.open(path, flags, callback)

  
  fs.appendFile(
      newFile, 'Hello content!', function (err) {
         if (err) throw err;
         console.log('Saved!');
  
  
  fs.appendFile(path, data[, options], callback)

    path <string> | <Buffer> | <URL> | <number> filename or file descriptor
    data <string> | <Buffer>
    options <Object> | <string>
        encoding <string> | <null> Default: 'utf8'
        mode <integer> Default: 0o666
        flag <string> See support of file system flags. Default: 'a'.
    callback <Function>
        err <Error>

fs.close(fd, callback)

fs.open('myfile', 'wx', (err, fd) => {
  if (err) {
    if (err.code === 'EEXIST') {
      console.error('myfile already exists');
      return;
    }

//===============================================================
*/

"use strict";
const fnAdd2 = function(a, b) {
   return new Promise(
         function(resolve,reject) {
             return resolve(a + b);         
         });
};

fnAdd2(3,4).
   then(function(c) {
      console.log(c);
   });
 
 
 //======================================

"use strict";
const fs = require('fs');

const fnMkdir = function(dossier) {
   return new Promise(
      function(resolve, reject) {
         return fs.mkdir(dossier,
            function(err) {
               if (err) {
                  return reject(err);
               }
               resolve();
            }
         );     
      }
   );
};


const fnOpen = function(fichier) {
   return new Promise(
      function(resolve, reject) {
         return fs.open(fichier, 'a+',
            function(err, fd) {
               if (err) {
                  return reject(err);
               }
               resolve(fd);
            }
         );
      }
   );
};



const dossier = 'dossier1';
const fichier = 'fichier1';
fnMkdir(dossier).
   then(function() {
      return fnOpen(dossier + '/' + fichier);
   }).
   then(function(fd) {
      console.log('fd', fd);
   }).
   catch(function(err) {
      console.log('error!', err);
   });


   

