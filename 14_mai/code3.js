
const a_multiplier = [2, 4, 6, 8];

const resultat = a_multiplier.reduce(
	function(precedent, element) {
		console.log(precedent, element);
		return precedent * element;	
	},
	1
);
console.log("resultat", resultat);




const reduce = function(tableau, func, valeur) {
	tableau[0] = 5;
	tableau.pop();
	tableau.pop();
	
	for (let i = 0; i < tableau.length; i++) {
		valeur = func(valeur, tableau[i]);
	}
	return valeur;
}

const f = function(p, e) { return p + e; };

const t = [3, 3, 3];
const a = reduce(t, f, 0);


console.log(t);
console.log(a);



const map = function(tableau, func) {
	const reponse = [];
	for (let i = 0; i < tableau.length; i++) {
		reponse.push(func(tableau[i]));
	}
	return reponse;
};

const mult_par_2 = function(x) {
	return x * 2;
};

const res = map([1,2,3], mult_par_2);
console.log(res); 
// [2,4,6]




const filter = function(tableau, func) {
	const reponse = [];
	for (let i = 0; i < tableau.length; i++) {
		if (func(tableau[i])) {
			reponse.push(tableau[i]);
		}	
	}
	return reponse;
};

const f = function(elem) {
	return elem > 3;
};


filter([1,2,3,4,5],f);
// [4,5]





const filter = function(tableau, func) {
	const reponse = [];
	tableau.forEach(
		function(elem) {
			if (func(elem)) {
				reponse.push(elem);
			}
		}
	);
	return reponse;
};

const f = function(elem) {
	return elem > 3;
};


filter([1,2,3,4,5],f);
// [4,5]




const forEach = function(tableau, func) {
	for (let i = 0; i < tableau.length; i++) {
		func(tableau[i]);
	}
}






















