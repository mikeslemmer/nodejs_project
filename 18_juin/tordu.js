const sum = x => y => z => x + y + z;


const sum = (x) => { return ( (y) => { return x + y; } ) }

const sum = function(x) { 
	return function(y) { 
		return x + y; 
	};
};


// returns the number 3
sum(2)(1); --> 3

sum(5) --> function(y) { return 5 + y; }

sum(5)(3) --> 8






