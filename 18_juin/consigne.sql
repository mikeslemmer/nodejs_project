
// Pour installer la base de donnees world.sql:

mysql --user=root --password=password < world.sql


// Chaque fois en utilisant un seul query

// 1. Trouver les 5 plus grandes villes de chaque pays et la somme des populations de ces 5 villes. S'il n'y a pas 5 villes
//    dans un pays, ne l'inclure pas. Comme ca:
// USA: 5000000
// FRA: 4000000





// 2. Trouver tous les pays qui ont le francais et l'anglais comme langue et determiner le moyen "life expectancy" pour
//    un pays avec chaque langue. Donc, s'il y a 3 pays ou on parle le francais, et les life expectancies sont 
//    75, 80 et 85, la reponse pour le francais serait 80.
//




// 3. Quel pays francophone a la ville capitale avec la population la plus grande?



	cl.countrycode, cl.language, city.name


SELECT 
	cl.countrycode, cl.language, city.name, MAX(city.population)
FROM 


	countrylanguage AS cl
INNER JOIN 
	country 
ON 
	cl.countrycode=country.code

INNER JOIN
	city
ON
	country.Capital=city.ID
	
	
WHERE 
	cl.language="French"
;

GROUP BY
	cl.language
;










