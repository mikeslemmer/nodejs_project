// mysql --user=root --password=mypassword


// Voir toutes les bases de donnees.
// SHOW DATABASES;

// Creer une base de donnees
// CREATE DATABASE `toto`;

// Travailler sur une base de donnees
// USE `toto`;
// aussi (un raccourci):
// \u toto

// Voir toutes les tables dans la db
// SHOW TABLES;

// Creer une table
CREATE TABLE `users` (
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255),
	`joined_date` DATETIME,
	`ideas` TEXT,	
	PRIMARY KEY(`id`)
);

// Voir les colonnes d'une table:
desc `users`;

// Voir tous les elements d'une table
SELECT * FROM `users`;

// Inserer un element
INSERT INTO `users` (
	`name`,
	`joined_date`,
	`ideas`
) VALUES (
	'Mike',	
	NOW(),
	'No ideas'
);

// Selectionner un element par id
SELECT * FROM `users` WHERE `id` = 1;

SELECT * FROM `users` WHERE `name` = 'Mike';
SELECT * FROM `users` WHERE `name` LIKE 'Mi%';
SELECT * FROM `users` WHERE `id` > 0;

// Supprimer un element
DELETE FROM `users` WHERE `id` = 5;

// Modifier un element
UPDATE `users` SET `name` = 'Mike 7' WHERE `id` = 1;


// Voir les index
SHOW INDEX FROM `users`;



// joindre (et limiter)
SELECT * FROM `city` INNER JOIN `country` ON `city`.`CountryCode` = `country`.`Code` LIMIT 1;

// voir que deux colonnes
 SELECT `country`.`name`, `city`.`name` FROM `city` INNER JOIN `country` ON `city`.`CountryCode` = `country`.`Code` LIMIT 1;

// select plus complique
SELECT `country`.`name` AS `country_name`, `city`.`name` AS `city_name`, `city`.`population` FROM `country` INNER JOIN `city` ON `city`.`CountryCode` = `country`.`Code` WHERE `country`.`name` = 'France' ORDER BY `country_name`, `city_name` ASC;

// Aggregation
SELECT SUM(`population`) FROM `city`;

// Tres complique 'query'
SELECT 
	`city`.`countrycode`, 
	`country`.`name`, 
	SUM(`city`.`population`) AS `sum` 
FROM 
	`city` 
INNER JOIN 
	`country` 
ON 
	`city`.`countrycode` = `country`.`code` 
WHERE 
	`country`.`name` like 'f%' 
GROUP BY 
	`city`.`countrycode` 
HAVING 
	`sum` > 10000 
ORDER BY `sum` DESC;





