const Controller = require('./controller');
const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const exphbs = require('express-handlebars');

app.use( bodyParser.json() ); 
app.use( express.static('public') );
app.use( express.static('node_modules/bootstrap/dist') );

app.engine('.hbs', exphbs( {defaultLayout: 'single', extname: '.hbs'} ));
app.set('view engine', '.hbs');

app.get('/', Controller.home);

app.get('/list', Controller.get_list);

app.get('/article', Controller.get_article);
app.post('/article', Controller.cree_article);
app.put('/article', Controller.modifie_article);
app.delete('/article', Controller.supprime_article);

//app.post();
//app.put();
//app.delete();

// Le port standard pour HTTP est 80
// Mais les ports < 1024 sont normalement reserves pour
// le super-user
app.listen(8000);	// port est entre 0 et 65535

