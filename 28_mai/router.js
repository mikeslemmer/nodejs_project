const Controller = require('./controller');
const express = require('express')();
const bodyParser = require('body-parser')

express.use( bodyParser.json() ); 

express.get('/list', Controller.get_list);

express.get('/article', Controller.get_article);
express.post('/article', Controller.cree_article);
express.put('/article', Controller.modifie_article);
express.delete('/article', Controller.supprime_article);

//express.post();
//express.put();
//express.delete();

// Le port standard pour HTTP est 80
// Mais les ports < 1024 sont normalement reserves pour
// le super-user
express.listen(8000);	// port est entre 0 et 65535

