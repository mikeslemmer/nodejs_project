class Model {	
	constructor() {
		this.data = require('./data');
	}
	
	// trouve tous les id des articles dans la
	// base de donnees
	// return: [1000, 1001]
	get_list() {
		// map, reduce, filter, forEach
		return this.data.articles.map(
			function(elem) {
				return elem.id;
			}
		);
	}

	get_article(id) {
		// balayer le tableau pour trouver l'element
		// avec elem.id === id. Si oui, cet element
		// sera dans le retour. Sinon, non.
		const filtre = this.data.articles.filter(		
			// La function qui decele
			function(elem) {
				return elem.id == id;
			}
		);
		
		// En fait toutes les trois expressions
		// font la meme chose
		return filtre[0] || null;
//		return filtre[0] ? filtre[0] : null;
//		return (filtre.length > 0) ? filtre[0] : null;
	}
	
	supprime_article(id) {
		/*
		// ca marche, mais ca recree tout un tableau
		this.data.articles = 
			this.data.articles.filter(
				function(elem) {
					return elem.id != id;
				}		
			);	
		*/
		
		
		const self = this;
		// on voit ca aussi:
		// const that = this
		this.data.articles.some(
			function(elem, indice, tableau) {			
				// si on essaye d'utiliser 'this' ici
				// ca ne marche pas parce qu'on es dans
				// le contexte d'une function et alors
				// 'this' refere a cette function pas a la
				// classe!
				//
				// ca ne marche pas:
				// this.data.articles[0]
				//
				// ca marche:
				// self.data.articles[0]
				//
				if (elem.id == id) {
					tableau[indice] = 
						tableau[tableau.length - 1];
					tableau.pop();
					// pop & push operent sur la fin du tableau
					// shift & unshift operent sur le debut					
					
					return true;
				}			
				return false;
			}
		);
	}
	
	cree_article(author, article) {
// ca ne marche pas parce qu'on ne sait pas que les id sont 
// en ordre
//		this.data.articles[this.data.articles.length - 1].id + 1		

/*
		// Pour creer des id qui descendent
		const new_id = -1 + this.data.articles.reduce(
			function(precedent, elem) {
				return elem.id < precedent ? elem.id : precedent;
			},
			1000000		// Pas Infinity parce que si le tableau est vide...
		);
*/
		const new_id = 1 + this.data.articles.reduce(
			function(precedent, elem) {
				return elem.id > precedent ? elem.id : precedent;
			},
			0
		);
		
		this.data.articles.push(
			{
				id: new_id,
				date: Math.floor(((new Date).getTime()) / 1000),
				article: article,
				auteur: author
			}
		);
		return new_id;
	}
	
	modifie_article(id, author, article) {
		this.data.articles.some(
			function(elem) {
				if (elem.id == id) {
					elem.auteur = author;
					elem.article = article;
					return true;				
				}
				return false;
			}		
		);
	}

}

/*
const model = new Model();
//const article = model.get_article(1005);
//console.log(article);
//const list = model.get_list();
//console.log(list);
const first = model.get_article(model.get_list()[0]);
console.log(first);

model.data.articles[0];
*/

module.exports = new Model();
