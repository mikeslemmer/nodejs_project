/* 
	"CRUD"	-- les operations qu'on fait souvent sur les bases de
					donnees
	"Create"
	"Retrieve"
	"Update"
	"Delete"
*/


/*
	"REST"
	GET		// acceder aux donnees
	POST		// creer des nouvelles donnees
	PUT		// changer des donnees "update"
	DELETE	// supprimer des donnees
	Ce sont les "verbes HTTP"
*/

const model = require('./model');

class Controller {

	constructor() {
		throw 'static class';
	}
	
	static get_list(request, response) {				
		response.send(model.get_list());
	}

	static get_article(request, response) {
		const id = request.query['id'];
		if (!id) {
			return response.sendStatus(403);
		}
		const article = model.get_article(id);
		if (!article) {
			return response.sendStatus(404);		
		}		
		response.send(article);		
	}
	
	// 1. id=1001&date=jeudi
	// 2. ['id=1001', 'date=jeudi']
	// 3. [ ['id', '1001' ], ['date', 'jeudi'] ]
	// 4. {
	//		id: '1001',
	//		date: 'jeudi'
	// }
	
	// esperluette == '&'
	// http://localhost:8000/article?id=1001&date=jeudi
	// curl -X DELETE http://localhost:8000/article?id=1000
	static supprime_article(request, response) {
		const id = request.query['id'];		
		if (!id) {
			return response.sendStatus(403);
		}
		if (!model.get_article(id)) {
			return response.sendStatus(404);			
		}
		
		model.supprime_article(id);
		response.send('supprimé');
	}
	
	
	// Creer un article et retourner l'id
	// exemple avec curl:
	// curl -X POST -d '{"author":"mike","article":"good article"}' -H 'content-type:application/json' http://localhost:8000/article
	static cree_article(request, response) {
		if (!request.body['author'] ||
		    !request.body['article']) {
			return response.sendStatus(403);
	   }

		const id = model.cree_article(
			request.body['author'],
			request.body['article']
		);
		
		// ici, le premier id c'est la cle pour l'objet
		// le second est la variable d'au-dessus
		response.send({ id: id });
	}

	// curl -X PUT -d '{"author":"mike","article":"bad article"}' -H 'content-type:application/json' http://localhost:8000/article?id=1001	
	static modifie_article(request, response) {
		const id = request.query['id'];		
		if (!id) {
			return response.sendStatus(403);
		}
		if (!model.get_article(id)) {
			return response.sendStatus(404);			
		}
		if (!request.body['author'] ||
		    !request.body['article']) {
			return response.sendStatus(403);
	   }
	   model.modifie_article(
	   	id, 
	   	request.body['author'],
			request.body['article']
		);
		response.sendStatus(200);
	}
}


module.exports = Controller;


