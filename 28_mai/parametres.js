	
	// 1. id=1001&date=jeudi
	// 2. ['id=1001', 'date=jeudi']
	// 3. [ ['id', '1001' ], ['date', 'jeudi'] ]
	// 4. {
	//		 id: '1001',
	//		 date: 'jeudi'
	//   }
	
const split_parametres = function(parametres) {
	console.log(parametres);
	const etape_1 = parametres.split('&');
	console.log(etape_1);
	const etape_2 = etape_1.map(
		function(elem) {
			return elem.split('=');
		}
	);
	console.log(etape_2);
	const etape_3 = etape_2.reduce(
		function(precedent, elem) {
			// elem = ['id', '1001']
			// elem[0] = 'id'
			// elem[1] = '1001'
			// precedent['id'] = '1001'
			// {id: '1001', date: 'jedi'}
			precedent[elem[0]] = elem[1];
			return precedent;
		},
		{}		
	);	
	console.log(etape_3);
	etape_3['id']
};

split_parametres('id=1001&date=jeudi&meteo=soleil');


/*
// d'autres exemples (plus moches)
let val = {};
tableau.forEach(
	function(elem) {
		val[elem[0]] = elem[1];
	}
);
// resultat en 'val'

for (let i = 0; i < tableau.length; i++) {
	val[tableau[i][0]] = val[tableau[i][1]];
}

*/


// en fait, la function qui est donne a forEach 
// a plusieurs parametres
const tab = ['a','b','c'];
tab.forEach(
	function(elem, indice) {
		console.log(elem, indice);
	}
);




