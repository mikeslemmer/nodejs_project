# pour voir une article, changer l'article, creer un nouveau article
echo
curl http://localhost:8000/article?id=1001
echo
curl -X PUT -d '{"author":"mike","article":"bad article"}' -H 'content-type:application/json' http://localhost:8000/article?id=1001
echo
curl http://localhost:8000/article?id=1001
echo
curl -X POST -d '{"author":"mike","article":"big article"}' -H 'content-type:application/json' http://localhost:8000/article
echo
curl http://localhost:8000/article?id=1002
echo
curl -X DELETE http://localhost:8000/article?id=1002
echo
curl http://localhost:8000/article?id=1002
