//======================================

"use strict";
 
const fnOpen = function(doss, fich) {
   return new Promise(
      function(resolve, reject) {   
         fs.open(doss + '/' + fich, 'a+', 
            function (err, fd) {
               if (err) {
                  return reject(err);
               }
               resolve(fd);   
            }
         );
      }
   );
};




fnOpen('doss1', 'fich1').

   then(function(fd) {
      fs.appendFile(
         fd, 'blablabla \n', 
         function(err) {
            console.log('1ere ligne ecrite dans le fichier');   
         }
      );
   }).
   
   then(function () {
      fs.close(
         fd, 
         function(){}
      );    
     }
   ).   
   
   catch(function(err) {
   
   }).
   
   finally(function() {
   
   });



try {
   // essaie

} catch(err) {
   // erreur

} finally {
   // toujours execute

}











new Promise().
   then().
   then().
   then().
   catch();

class Promise {
   constructor {
   
   }

   then(fn) {
      // params viennent du then precedent
      const ret = fn(params);
      if (typeof ret == 'Promise') {
         ret.then(function(parametres) {
            // Appelle le prochain 'then' avec les parametres
         });
      } else {
         // Appelle le prochain 'then' avec ret comme parametre
      }
      return this;
   }
   
   catch() {
      // faire des operations
      return this;   
   }
   
   finally() {
      
   }
}

const promise = new Promise();
promise.then().then().then().then();





new Promise(
     function(resolve, reject) {
         resolve(1);
     }
   ).
   
   then(function(toto) { 
      if (toto == 1) {
         return 1;      
      } else {   
         return new Promise(function(resolve, reject) {
            setTimeout(function() { resolve(7); }, 3000);
         });
      }
   }).

   then(
      function(x) {
         return x * 5;
      }
   ).

   then(function(y) {
      console.log(y);
   });





class CreerTableau {
   constructor() {
      this.tab = [];   
   }
   
   ajoute(x) {
      this.tab.push(x);
      return this;
   }
}


const ct = new CreerTableau();
ct.
   ajoute(function(){ return 3; }).
   ajoute(function(){ return 'toto'; }).
   ajoute(function(){ return 6; }).
   ajoute(function(){ return null; });

