
1. Cree une fonction qui additione deux nombres et appelle
   une fonction avec la reponse des qu'elle a fini.
   A. Avec callback
   B. Avec Promise
   C. Avec async/await

   ex sync:
   const fn = function(a, b) { return a + b; };


const callback_fn = function(params, callback) {
   // Faire des operations avec les params
   callback(/*resultat*/);
};


A.

const foncAdd = function(a,b, afficheSomme) {
   const somme = a + b;
   afficheSomme(somme);
};

const fn = function(somme) {
   console.log(somme);
}

foncAdd(5, 7, fn);


B.

const funcAdd2 = function(a,b) {
   let somme2= a+b;
   affSomme2().
      then(function() {
         console.log(somme2);
      }); 
   
};


const funcAdd2 = function(a, b) {
   return new Promise(
      function(resolve, reject) {
         resolve(a + b);
      }
   );
};


funcAdd2(5, 7).
   then(function(resultat) {
      console.log(resultat);
   });
   
   
   
C.
   const funcAdd3 = async function(a, b) {
      return new Promise(function(resolve, reject) {
         resolve(a + b);      
      });
   }

   const main = async function() {
      const c = await funcAdd3(5, 7);
      console.log(c);      
      
      
      funcAdd3(5, 7).then(function(c) {
         console.log(c);
      });
   }

   main();


   
2. Fait la meme chose mais avec (a / b) - et il y a une 
   erreur si b == 0. Gere-ca avec reject pour la promise et
   avec un deuxieme callback pour les callbacks. Et j'ai
   aucune idee comment ca marche pour async/await
   
   (note: pour les callbacks la norme est: function(err, result) {})
   
   
3. Cree une fonction qui:
   A. Cree un repertoire
   B. Cree un fichier dans le repertoire
   C. Enregistre une phrase dans le fichier
   D. Ferme le fichier
   E. Rouvre le fichier et lis le contenu
   F. Imprime le contenu sur l'ecran
   G. Supprime le fichier
   H. Supprime le repertoire.
   
// Si tu veux, fais-ca avec le pyramid of doom et apres avec promise et async/await   
// Sinon, fais-le avec que un systeme (tu peux choisir)




