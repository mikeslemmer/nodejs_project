
// f1: c <- a + b
// f2: e <- c * d
// f3: g <- e / f

const f1 = function(a, b) {
   return a + b;
}

const f2 = function(c, d) {
   return c * d;
}

const f3 = function(e, f) {
   return e / f;
}

const fonction = function(a, b, d, f) {

   const g = f3( f2( f1(a, b), d), f);   
   return g;
}


// Asynchronous

// Callbacks

request.get("url", 
   function(erreur, resultat) {
      request.get("url2", 
         function(err, res) {
            request.get("url3",
               function(err, res) {
               
               }
            );      
         }
      );
   }
);



"use strict";
const f1 = function(a, b) {
   return new Promise(
      function(resolve, reject) {
         if (a == 0) {
            return reject('a cant be zero');         
         }
         resolve(a + b);      
      }
   );
};

const f2 = function(c, d) {
   return new Promise(
      function(resolve, reject) {
         resolve(c * d);      
      }
   );
};

const f3 = function(e, f) {
   return new Promise(
      function(resolve, reject) {
         resolve(e / f);      
      }
   );
};

const fonction = function(a, b, d, f) {
   const g = 
      f1(a,b)
      .then(function(r1) {
         return f2(r1, d);
      })
      .then(function(r2) {
         return f3(r2, f);
      })
      .then(function(r3) {
         console.log("SUCCES", r3);
      })
      .catch(function(err) {
         console.log("ERREUR", err);
      })
      .finally(function() { 
         console.log("Finally!");        
      });
   return g;
}




const request = require('request');

const urls = [
   'http://www.google.com',
   'http://www.orange.fr',
   'http://www.yahoo.fr'
];

const telecharge = function(url) {
   return new Promise(function(resolve, reject) {
      request.get(url, function(error, result) {
         if (error) {
            return reject(error);         
         }
         resolve(result);
      });
   });
};

const promises = urls.map(
   function(elem) {
      return telecharge(elem); 
   }
);

let res;
Promise.all(promises).then(
   function(resultat) {
      res = resultat;
   }
);








// Synchronous
try {
   const toto = f1(a, b);
   const toto2 = f2(toto, d);
   f3(toto2, f);
}
catch(err) {
   gere_une_erreur
}
finally {
   operation
}




//////

const fn = function() {
   console.log('Z');
};

console.log('X');
setTimeout(fn, 1000);
console.log('Y');



  
const addition_delai = function(a, b, callback) {
   setTimeout(
      function() { 
         callback(a + b);   
      },
      1000
   );
};



addition_delai(5, 7, function(c) {
   console.log(c)
});


///



const addition_delai_promise = function(a, b) {
   return new Promise(
      function(resolve, reject) {
         setTimeout(
            function() { 
               resolve(a + b);   
            },
         1000);
      }
   );
};



addition_delai_promise(5, 7).
   then(function(c) {
     console.log(c); 
   });


////////

const main = async function() {
   const c = await addition_delai_promise(5, 7);
   console.log(c);
}
main();










